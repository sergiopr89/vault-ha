#!/bin/bash

DEFAULT_NAME=vault-poc
SEEP_TIME_SECS=60
KEYS_FILE=cluster-keys.json

if [[ -z ${KLUSTER_NAME} ]]; then
  export KLUSTER_NAME=${DEFAULT_NAME}
fi

if [[ -z ${RELEASE_NAME} ]]; then
  export RELEASE_NAME=${DEFAULT_NAME}
fi

# Start/Recreate cluster
KinD/create_kind_cluster.sh
kubectl config use-context kind-${KLUSTER_NAME}

# Install Helm Release
helm dependency update helm/vault-poc
helm install ${RELEASE_NAME} helm/vault-poc

# Non-deterministic wait for helm release to start all the vault pods. TODO: You know man...
echo "Please, wait for ${SEEP_TIME_SECS} to start the Vault service"
sleep ${SEEP_TIME_SECS}
echo "DONE... hope so"

# Unseal VAULT
mkdir -p tmp/
cd tmp/
kubectl exec ${RELEASE_NAME}-0 -- vault operator init -key-shares=1 -key-threshold=1 -format=json > ${KEYS_FILE}
VAULT_UNSEAL_KEY=$(cat ${KEYS_FILE} | jq -r ".unseal_keys_b64[]")
VAULT_REPLICAS=$(kubectl get pods | grep -E "^${RELEASE_NAME}-[0-9]+" | awk '{print $1}')
for REPLICA in ${VAULT_REPLICAS}; do
  kubectl exec ${REPLICA} -- vault operator unseal ${VAULT_UNSEAL_KEY}
done

echo "DONE"
