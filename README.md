# Pre-requisites
* A Linux based distribution for running the PoC
* The jq cli command
* [Git client (cli)](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [Docker](https://docs.docker.com/get-docker/)
* [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
* [KinD](https://kind.sigs.k8s.io/docs/user/quick-start/)
* [Helm3](https://helm.sh/docs/intro/install/)

# Getting the project
Open a terminal and create a working directory for your PoC, like:
```
git clone $REPOURL vault-poc
cd vault-poc
```

# Bootstrap the environment with Consul and Vault inside KinD
Run the following command:
```
./bootstrap.sh
```

# Expose the Consul (8081) and Vault (8080) web ports
For Consul:
```
kubectl port-forward svc/vault-poc-consul-ui 8081:80 >/dev/null &
```

For Vault:
```
kubectl port-forward svc/vault-poc-active 8080:8200 >/dev/null &
```

# Get the token to login in the Vault web UI
Run the following command:
```
./get_root_token.sh
```
It will print the generated token you have to set as token in the Vault login web UI.
